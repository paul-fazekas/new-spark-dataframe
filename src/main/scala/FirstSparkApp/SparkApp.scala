package FirstSparkApp

//import statements --------------------------------------------------------------------
import org.apache.spark.sql.SparkSession
//import ability to take user input
import scala.io.StdIn._
//used to delete directory after use
import scala.reflect.io.File
//necessary to import log4j to be able to override default imports
import org.apache.log4j.{Level, Logger}
//imported expressions.Window to allow adding index column in autos example DataFrame -df
import org.apache.spark.sql.expressions.Window


//Object --------------------------------------------------------------------------------
object SparkApp {
  // Tells application to log Error and above instead of all INFO statements
  Logger.getLogger("org").setLevel(Level.ERROR)


  //Functions-------------------------------------------------------------------------------
  def main(args: Array[String]): Unit = {

    //session builder
    val spark: SparkSession = SparkSession.builder()
      .master("local[*]")
      .appName("SparkByExample")
      .getOrCreate()
    println("But overrode info logging to hide non pertinent data---------------------------------------")

    //used later when deleting parquet directory after use
    def deleteDirectory(directory: String): Unit = {
      val dir = File(directory)
      if (dir.isDirectory && dir.exists) {
        dir.deleteRecursively()
      }
    }

    //set of imports that have to be inside main------------------------------------------
    import spark.sqlContext.implicits._ //for sorting
    //added import of sql functions to allow for orderBy sorting
    import org.apache.spark.sql.functions._ // allows to format like (desc("col 1")) instead of  ($"Col 1".desc)


    // Originally had explicit file path, but shortened it to default directory, filePath2 adds two additional folders to navigate
    val filePath2 = "OneDeep/TwoDeep/"


    //Chaining multiple options
    val df1 = spark.read.format("csv").option("header", "true").load("students.csv")
    val df2 = spark.read.format("csv").option("header", "true").load("moreStudents.csv")
    //this was a suggestion I found online, but mapping through takes a lot of extra time that is not needed.
    //val df2 = spark.read.options(Map("inferSchema"->"true","sep"->",","header"->"true")).csv(filePath + "moreStudents.csv")
    val df3 = spark.read.format("csv").option("header", "true").load("addStudents.csv")
    val df4 = df1.union(df2).union(df3)

    //displaying DFs
    println("\nShows DF made from 'students' file")
    df1.show()
    println("Shows DF made from 'moreStudents' file")
    df2.show()
    println("Shows DF made from 'addStudents' file")
    df3.show()

    //Gets input from user to display set number of records
    println("The DataFrame with the combined data for all has " + df4.count() + " records in it.")
    println("How many records would you like to see? ")
    val noLines = readInt()
    println("The DataFrame will show up to " + noLines + " Students.")

    // showing full tables before and after sort.
    df4.show(noLines) // wanted to make sure that all rows would show, left room for a few additions.
    if (df4.count() <= noLines)
      println("Showing all " + df4.count() + " records.")

    println("\nShowing table sorted by Age and GPA")
    df4.sort($"Age".desc, $"GPA".desc).show(noLines)
    if (df4.count() <= noLines)
      println("Showing all " + df4.count() + " sorted records.")


    // next set of parameters-------------------------------------------------------------------

    // took common path from other info and only directed to what is new
    val df = spark.read.format("csv").option("header", "true").load(filePath2 + "Autos.csv")
    println("\n\nDF created from file in a different folder in different location")
    df.show()
    println("\n Sorted using orderBy for autos file")
    df.orderBy(desc("MPG")).show()
    println("Added a column to index the rows printed.")
    df.withColumn("id", row_number().over(Window.orderBy(desc("MPG")))).show()

    //will write parquet file based on df DataFrame info
    df.write.mode("append").parquet("testParquet") // originally had append as a mode, but script now deletes parquet file after each use


    println("Previous record written to Parquet file which was picked up and displayed here:")
    println("No id index is listed because it was an added column during display, not permanent record")
    println("Sorted by 'MPG' column")
    val df7 = spark.read.parquet("testParquet")
    df7.sort($"MPG".desc).show()
    deleteDirectory("testParquet") //deletes directory containing parquet files after use.


    // new set of data read------------------------------------------------------------------
    println("\n New table read from json formatted file - each line is one json with no extra formatting.")
    println("Each json line is 'column name: row info'. Misspelling will cause" +
      " \nanother column to be formed with null info")
    println("\nColumns may not be displayed in order they were saved. Listed alphabetically?")
    println("*Had to put a space in front of 'Name' to get it to populate in first column - sort by 'MPG'")
    val df8 = spark.read.json("NewJSON.json")
    df8.show()
    println("There are " + df8.count() + " entries to display.")
    // end----------------------------------------------------------------------------------
    println("\nEnd of Program")
    println("\n\n-------------------------------------------------------------------------------------")

  }
}